package com.example.Exam.sevices;

import java.util.List;
import java.util.Optional;

import com.example.Exam.entity.Question;
import com.example.Exam.entity.Test;
import com.example.Exam.entity.TestType;

public interface AllSevices {

    // QUESTION
    List<Question> getListQuestions();

    Question addQuestion(Question question);

    Question updateQuestion(Question question);

    void deleteQuestion(Question question);

    Optional<Question> gettQuestion(int questionId);

    // TEST
    List<Test> getListTests();

    Test addTest(Test test);

    Test updateTest(Test test);

    void deleteTest(Test test);

    Optional<Test> getTest(String testId);

    // TEST_TYPE
    List<TestType> getListTestType();

    TestType addTestType(TestType testType);

    TestType updateTestType(TestType testType);

    void deleteTestType(TestType testType);

    Optional<TestType> getTestType(String testTypeId);
}