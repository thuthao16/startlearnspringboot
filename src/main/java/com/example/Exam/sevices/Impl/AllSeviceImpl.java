package com.example.Exam.sevices.Impl;

import java.util.List;
import java.util.Optional;

import com.example.Exam.entity.Question;
import com.example.Exam.entity.Test;
import com.example.Exam.entity.TestType;
import com.example.Exam.repositories.QuestionRepository;
import com.example.Exam.repositories.TestRepository;
import com.example.Exam.repositories.TestTypeRepository;
import com.example.Exam.sevices.AllSevices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AllSeviceImpl implements AllSevices {

    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private TestRepository testRepository;
    @Autowired
    private TestTypeRepository testTypeRepository;

    // QUESTION
    @Override
    public List<Question> getListQuestions() {
        return questionRepository.findAll();
    }

    @Override
    public Question addQuestion(Question question) {
        return questionRepository.save(question);
    }

    @Override
    public Question updateQuestion(Question question) {
        return questionRepository.save(question);
    }

    @Override
    public void deleteQuestion(Question question) {
        questionRepository.delete(question);
    }

    @Override
    public Optional<Question> gettQuestion(int questionId) {
        return questionRepository.findById(questionId);
    }

    // TEST

    @Override
    public List<Test> getListTests() {
        return testRepository.findAll();
    }

    @Override
    public Test addTest(Test test) {
        return testRepository.save(test);
    }

    @Override
    public Test updateTest(Test test) {
        return testRepository.save(test);
    }

    @Override
    public void deleteTest(Test test) {
        testRepository.delete(test);
    }

    @Override
    public Optional<Test> getTest(String testId) {
        return testRepository.findById(testId);
    }

    // TEST_TYPE

    @Override
    public List<TestType> getListTestType() {
        return testTypeRepository.findAll();
    }

    @Override
    public TestType addTestType(TestType testType) {
        return testTypeRepository.save(testType);
    }

    @Override
    public TestType updateTestType(TestType testType) {
        return testTypeRepository.save(testType);
    }

    @Override
    public void deleteTestType(TestType testType) {
        testTypeRepository.delete(testType);
    }

    @Override
    public Optional<TestType> getTestType(String testTypeId) {
        return testTypeRepository.findById(testTypeId);
    }

}