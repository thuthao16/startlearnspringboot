package com.example.Exam.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import com.example.Exam.entity.Question;
import com.example.Exam.repositories.QuestionRepository;
import com.example.Exam.sevices.AllSevices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
// @RequestMapping("/")
public class AllController {

    @Autowired
    private AllSevices allSevices;

    @Autowired
    private QuestionRepository questionRepository;

    // @RequestMapping(value = "/question-management", method = RequestMethod.GET)
    // public String getQuestions(Model model) {
    //     model.addAttribute("questions", allSevices.getListQuestions());
    //     return "question-management";
    // }

    @RequestMapping(value = "/question-management", method = RequestMethod.GET)
    public String getQuestions(Model model, HttpServletRequest request) {

        int page = 0; // default page number is 0 (yes it is weird)
        int size = 2; // default page size is 10

        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }
        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }
        model.addAttribute("questions", questionRepository.findAll(PageRequest.of(page, size)));

        return "question-management";
    }

    @RequestMapping("/create-question")
    public String createQuestion(Model model) {
        Question question = new Question();
        model.addAttribute("questionForm", question);
        return "add-question";
    }

    // @RequestMapping(value = "/addquestion", method = RequestMethod.POST)
    @PostMapping("/addquestion")
    public String insertQuestion(@ModelAttribute("questionForm") Question question, Model model) {
        int questionId = question.getQuestionId();
        String content = question.getContent();
        String answer1 = question.getAnswer1();
        String answer2 = question.getAnswer2();
        String answer3 = question.getAnswer3();
        String correctAnswer = question.getCorrectAnswer();
        if (content != null && answer1 != null && answer2 != null && answer3 != null && correctAnswer != null) {
            Question questionn = new Question();
            questionn.setQuestionId(questionId);
            questionn.setAnswer1(answer1);
            questionn.setAnswer2(answer2);
            questionn.setAnswer3(answer3);
            questionn.setContent(content);
            questionn.setCorrectAnswer(correctAnswer);

            allSevices.addQuestion(questionn);
            return "redirect:/question-management";
        }
        model.addAttribute("errormessage", "Vui long khong de trong");
        return "add-question";
    }

    @RequestMapping("/delete-question/{questionId}")
    public String deleteQuestion(@PathVariable("questionId") int id, Model model) {
        Optional<Question> questionOpt = allSevices.gettQuestion(id);
        Question question = questionOpt.get();
        allSevices.deleteQuestion(question);
        return "redirect:/question-management";
    }

    @RequestMapping("/update-question/{questionId}")
    public String updateQuestion(@PathVariable("questionId") int id, Model model) {
        Optional<Question> questionOpt = allSevices.gettQuestion(id);
        Question question = questionOpt.get();

        int questionId = question.getQuestionId();
        String content = question.getContent();
        String answer1 = question.getAnswer1();
        String answer2 = question.getAnswer2();
        String answer3 = question.getAnswer3();
        String correctAnswer = question.getCorrectAnswer();

        if (content != null && answer1 != null && answer2 != null && answer3 != null && correctAnswer != null) {
            Question questionn = new Question();
            questionn.setQuestionId(questionId);
            questionn.setAnswer1(answer1);
            questionn.setAnswer2(answer2);
            questionn.setAnswer3(answer3);
            questionn.setContent(content);
            questionn.setCorrectAnswer(correctAnswer);

            // allSevices.updateQuestion(question);
            model.addAttribute("questionForm", questionn);
            return "add-question";
        }
        model.addAttribute("errormessage", "Vui long khong de trong");
        return "update-question";
    }

}