package com.example.Exam.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "test", schema = "example")
public class Test {
    @Id
    @Column(name = "test_id", unique = true)
    private String testId;
    @Column(name = "active")
    private int active;
    @Column(name = "description")
    private String description;
    @Column(name = "password")
    private String password;
    @Column(name = "question_number")
    private int questionNumber;
    @Column(name = "test_name")
    private String testName;
    @Column(name = "test_time")
    private int testTime;
    
    @ManyToOne
    @JoinColumn(name = "test_type_id")
    private TestType testType;

    @ManyToMany(mappedBy = "tests")
    private Set<Question> questions;

    
    public Test() {
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public int getTestTime() {
        return testTime;
    }

    public void setTestTime(int testTime) {
        this.testTime = testTime;
    }

    public TestType getTestType() {
        return testType;
    }

    public void setTestType(TestType testType) {
        this.testType = testType;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

}