package com.example.Exam.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "test_type", schema = "example")
public class TestType {
    @Id
    @Column(name = "test_type_id", unique = true)
    private String testTypeId;
    @Column(name = "test_type_name")
    private String testTypeName;

    @OneToMany(mappedBy = "testType")
    private Set<Test> tests;

    public TestType() {
    }

    public String getTestTypeId() {
        return testTypeId;
    }

    public void setTestTypeId(String testTypeId) {
        this.testTypeId = testTypeId;
    }

    public String getTestTypeName() {
        return testTypeName;
    }

    public void setTestTypeName(String testTypeName) {
        this.testTypeName = testTypeName;
    }

    public Set<Test> getTests() {
        return tests;
    }

    public void setTests(Set<Test> tests) {
        this.tests = tests;
    }

}