package com.example.Exam.repositories;

import com.example.Exam.entity.Question;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier(value = "questionRepository")
public interface QuestionRepository extends JpaRepository<Question, Integer> {
}