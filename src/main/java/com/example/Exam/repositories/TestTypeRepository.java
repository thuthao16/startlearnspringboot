package com.example.Exam.repositories;

import com.example.Exam.entity.TestType;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier(value = "testTypeRepository")
public interface TestTypeRepository extends JpaRepository<TestType, String> {
}