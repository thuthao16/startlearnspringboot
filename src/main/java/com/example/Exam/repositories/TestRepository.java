package com.example.Exam.repositories;

import com.example.Exam.entity.Test;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier(value = "testRepository")
public interface TestRepository extends JpaRepository<Test, String>{
}